# PMT-VIEW

Web app do display live the PMT sensor 
output.


To execute it run 
```
python3 pmt-view/main.py  --port 5321
```

A web browser will open with the live monitor of the Joules/Watts usage.
